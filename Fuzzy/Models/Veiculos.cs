﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Fuzzy.Models
{
    public class Veiculos
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Angulo { get; set; }

        private double DistanciaEntreDoisPontos(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow((x1 - x2), 2) + Math.Pow((y1 - y2), 2));
        }

        public void Andar(double direcao)
        {
            Angulo += direcao;
            X += Math.Cos(Angulo * Math.PI / 180) * 2;
            Y += Math.Sin(Angulo * Math.PI / 180) * 2; //Tenho dúvidas quanto a isso aushauhsuahs depois é bom rever isso sem sono
        }

    }
}
