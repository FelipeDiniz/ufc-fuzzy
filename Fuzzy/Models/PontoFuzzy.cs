﻿namespace Fuzzy.Models
{
    public class PontoFuzzy
    {
        public PontoFuzzy() { }
        public PontoFuzzy(string nome, double a, double c, double d, double b)
        {
            Nome = nome;
            A = a;
            B = b;
            C = c;
            D = d;
        }

        public string Nome { get; set; }
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public double D { get; set; }

        public double PertinenciaPonto(double coordenada)
        {
            //ponto não pertence à area trapezoidal. Note que B > A.  a /c|-----d|b\
            if (coordenada > B || coordenada < A)
                return 0;
            //Trecho constante
            else if (coordenada >= C && coordenada <= D) //Note que esse sinal de igualdade vai ser útil em situações aondeo gráfico deve ser linear ( \ )
                return 1;
            //Trecho crescente
            else if (coordenada < C)
                return (1 / (C - A)) * (coordenada - A);
            //Trecho descrescente
            else
                return (1 / (D - B)) * (coordenada - B);
        }

    }
}
