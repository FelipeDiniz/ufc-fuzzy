﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuzzy.Models
{
    public class PontosOrientacaoFuzzy
    {
        public PontosOrientacaoFuzzy(string nome, double a, double b, double c)
        {
            Nome = nome;
            A = a;
            B = b;
            C = c;
        }

        public string Nome { get; set; }
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }

        public double PertinenciaPonto(double coordenada)
        {
            if(coordenada < (A + B) / 2)
                return (2 / ( B - A) ) * (coordenada - A);  
            else
                return (2 / (A - B)) * (coordenada - B);
        }

    }
}
