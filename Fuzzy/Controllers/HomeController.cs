﻿using Fuzzy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fuzzy.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            /*pontos do para posição x do veículo*/
            PontoFuzzy left = new PontoFuzzy("LE", 0, 0, 10, 35);
            PontoFuzzy leftCenter = new PontoFuzzy("LC", 30, 40, 40, 50);
            PontoFuzzy center = new PontoFuzzy("CE", 45, 50, 50, 55);
            PontoFuzzy rightCenter = new PontoFuzzy("RC", 50, 60, 60, 70);
            PontoFuzzy right = new PontoFuzzy("RI", 65, 90, 100, 100);

            PontoFuzzy[] pontosFuzzyPosicao = { left, leftCenter, center, rightCenter, right };
            Tuple<Veiculos, PontoFuzzy[]> tuple = new Tuple<Veiculos, PontoFuzzy[]>(new Veiculos(), pontosFuzzyPosicao);

            return View(tuple);
        }

        [HttpPost]
        public ActionResult Index(Veiculos veiculo)
        {

            /* pontos do para posição x do veículo */
            PontoFuzzy left = new PontoFuzzy("LE", 0, 0, 10, 35);
            PontoFuzzy leftCenter = new PontoFuzzy("LC", 30, 40, 40, 50);
            PontoFuzzy center = new PontoFuzzy("CE", 45, 50, 50, 55);
            PontoFuzzy rightCenter = new PontoFuzzy("RC", 50, 60, 60, 70);
            PontoFuzzy right = new PontoFuzzy("RI", 65, 90, 100, 100);
            PontoFuzzy[] pontosFuzzyPosicao = { left, leftCenter, center, rightCenter, right };
            Tuple<Veiculos, PontoFuzzy[]> tuple = new Tuple<Veiculos, PontoFuzzy[]>(veiculo, pontosFuzzyPosicao);

            if (veiculo.X > 100 || veiculo.X < 0)
            {
                TempData["MensagemErro"] = "O universo de discurso da variável x é [0, 100]";
                return View(tuple);
            }
            if (veiculo.Angulo > -90 || veiculo.Angulo < 270)
            {
                TempData["MensagemErro"] = "O universo de discurso da variável angulo é [-90, 270]";
                return View(tuple);
            }

            if (veiculo.Y >= 95)
            {
                TempData["MensagemSucesso"] = "Veículo estacionado com sucesso";
                return View(tuple);
            }

            /* Pontos do para o angulo (orientação) do veículo
               Levo em consideração aqui que uma triangular pode ser resolvida como trapezoidal igualando os valores de C e D :)
             */
            PontoFuzzy rightBellow = new PontoFuzzy("RB", -100, -45, -45, 10);
            PontoFuzzy rightUpper = new PontoFuzzy("RU", -10, -35, -35, 60);
            PontoFuzzy rightVertical = new PontoFuzzy("RV", 45, -67.5, -67.5, 90);
            PontoFuzzy vertical = new PontoFuzzy("VE", 80, 90, 90, 100);
            PontoFuzzy leftVertical = new PontoFuzzy("LV", 90, 112.5, 112.5, 135);
            PontoFuzzy leftUpper = new PontoFuzzy("LU", 120, 155, 155, 190);
            PontoFuzzy leftBellow = new PontoFuzzy("LB", 170, 225.5, 225.5, 270);
            PontoFuzzy[] pontosFuzzyOrientacao = { rightBellow, rightUpper, rightVertical, vertical, leftVertical, leftUpper, leftBellow };

            KeyValuePair<string, double>[,] codigosDosValoresDeDirecao = new KeyValuePair<string, double>[7, 5] {
                { new KeyValuePair<string, double>("PS", 0), new KeyValuePair<string, double>("PM", 0), new KeyValuePair<string, double>("PM", 0), new KeyValuePair<string, double>("PM", 0), new KeyValuePair<string, double>("PB", 0) },
                { new KeyValuePair<string, double>("NS", 0), new KeyValuePair<string, double>("PS", 0), new KeyValuePair<string, double>("PM", 0), new KeyValuePair<string, double>("PB", 0), new KeyValuePair<string, double>("PB", 0) },
                { new KeyValuePair<string, double>("NM", 0), new KeyValuePair<string, double>("NS", 0), new KeyValuePair<string, double>("PS", 0), new KeyValuePair<string, double>("PM", 0), new KeyValuePair<string, double>("PB", 0) },
                { new KeyValuePair<string, double>("NM", 0), new KeyValuePair<string, double>("NM", 0), new KeyValuePair<string, double>("ZE", 0), new KeyValuePair<string, double>("PM", 0), new KeyValuePair<string, double>("PM", 0) },
                { new KeyValuePair<string, double>("NB", 0), new KeyValuePair<string, double>("NM", 0), new KeyValuePair<string, double>("NS", 0), new KeyValuePair<string, double>("PS", 0), new KeyValuePair<string, double>("PM", 0) },
                { new KeyValuePair<string, double>("NB", 0), new KeyValuePair<string, double>("NB", 0), new KeyValuePair<string, double>("NM", 0), new KeyValuePair<string, double>("NS", 0), new KeyValuePair<string, double>("PS", 0) },
                { new KeyValuePair<string, double>("NB", 0), new KeyValuePair<string, double>("NB", 0), new KeyValuePair<string, double>("NM", 0), new KeyValuePair<string, double>("NM", 0), new KeyValuePair<string, double>("NS", 0) }
            };

            PontoFuzzy negativeBig = new PontoFuzzy("NB", -30, -30, -30, -15);
            PontoFuzzy negativeMedium = new PontoFuzzy("NM", -25, -15, -15, -5);
            PontoFuzzy negativeSmall = new PontoFuzzy("NS", -12, -6, -6, 0);
            PontoFuzzy zero = new PontoFuzzy("ZE", -5, 0, 0, 5);
            PontoFuzzy positiveSmall = new PontoFuzzy("PS", 0, 6, 6, 12);
            PontoFuzzy positiveMedium = new PontoFuzzy("PM", 5, 15, 15, 25);
            PontoFuzzy positiveBig = new PontoFuzzy("PB", 18, 30, 30, 30);
            PontoFuzzy[] pontosFuzzyDirecao = { negativeBig, negativeMedium, negativeSmall, zero, positiveSmall, positiveMedium, positiveBig };

            //Agora damos os valores ara cada elemento da matriz de informação de resultados(codigosDosValoresDeDirecao) que contém valores "and" (mínimo)
            for (int i = 0; i < 7; i++)
            {
                double pertinenciaOrientacao = pontosFuzzyOrientacao[i].PertinenciaPonto(veiculo.Angulo);
                for (int j = 0; j < 5; j++)
                {
                    double pertinenciaPosicaoX = pontosFuzzyPosicao[j].PertinenciaPonto(veiculo.X);
                    double valor = pertinenciaOrientacao < pertinenciaPosicaoX ? pertinenciaOrientacao : pertinenciaPosicaoX;
                    codigosDosValoresDeDirecao[i, j] = new KeyValuePair<string, double>(codigosDosValoresDeDirecao[i, j].Key, valor);
                }
            };

            //Defuzzificando: CENTROID
            double divisor = 0, dividendo = 0;
            for (double i = -30; i <= 60; i += 0.25)
            {
                int j = PontoOuDePertinencia(pontosFuzzyDirecao, i);
                double pertinenciaAtual = pontosFuzzyDirecao[j].PertinenciaPonto(i);

                for (int m = 0; m < 7; m++)
                    for (int n = 0; n < 5; n++)
                        if (codigosDosValoresDeDirecao[m, n].Key == pontosFuzzyDirecao[j].Nome)
                            if (codigosDosValoresDeDirecao[m, n].Value == 0)
                                pertinenciaAtual = 0;
                            else
                                pertinenciaAtual = codigosDosValoresDeDirecao[m, n].Value > pertinenciaAtual ? codigosDosValoresDeDirecao[m, n].Value : pertinenciaAtual;
                if (pertinenciaAtual * i != 0)
                {
                    divisor += pertinenciaAtual;
                    dividendo += pertinenciaAtual * i;
                }

            }

            double valorDefuzzificado = dividendo / divisor;
            ;

            //int indexA = PontoOuDePertinencia(pontosFuzzyPosicao, x);
            //int indexB = PontoOuDePertinencia(pontosFuzzyDirecao, angulo);
            //string valorDirecao = codigosDosValoresDeDirecao[indexA, indexB];
            //PontoFuzzy direcaoMaisPertinente = pontosFuzzyDirecao.Where(p => p.Nome == valorDirecao).Single();

            double direcao = 60 * valorDefuzzificado - 30; //100% daria 60 mas meu universo começo de -30, por isso esse -30 aí...
            veiculo.Andar(direcao);
            
            return View(tuple);
        }

        //Aqui eu pego um ou de para cada ponto... determinado ponto fuzzy pode ser, por exemplo, left ou left center, sendo asim eu pego o maior valor
        public int PontoOuDePertinencia(PontoFuzzy[] pontosFuzzy, double coordenada)
        {
            int pontoMaisPertinente = 0;
            for (int i = 1; i < pontosFuzzy.Count(); i++)
            {
                if (pontosFuzzy[i].PertinenciaPonto(coordenada) > pontosFuzzy[pontoMaisPertinente].PertinenciaPonto(coordenada))
                    pontoMaisPertinente = i;
            }
            return pontoMaisPertinente;
        }

    }
}